import React, { useEffect, useState } from 'react';
import axios from 'axios';

interface ProfileData {
    id: number;
    name: string;
    description: string;
    hobbies: string[];
}

const Profile: React.FC = () => {
    const [profile, setProfile] = useState<ProfileData | null>(null);

    useEffect(() => {
        axios.get('http://localhost:4000/profile/1').then((response) => {
            setProfile(response.data);
        });
    }, []);

    if (!profile) return <p>Lädt...</p>;

    return (
        <div style={{ maxWidth: '600px', margin: '0 auto', padding: '20px' }}>
            <h1>{profile.name}</h1>
            <p>{profile.description}</p>
            <h3>Hobbies:</h3>
            <ul>
                {profile.hobbies.map((hobby, index) => (
                    <li key={index}>{hobby}</li>
                ))}
            </ul>
        </div>
    );
};

export default Profile;
